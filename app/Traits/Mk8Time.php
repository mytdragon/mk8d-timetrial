<?php
namespace App\Traits;

use Carbon\Carbon;
use App\Exceptions\Mk8TimeException;

trait Mk8Time
{
    protected $mk8TimeFormat = 'i:s.v';
    protected $mk8TimeRegex = '/^([0-9]{1}):([0-9]{2}).([0-9]{3})$/';
    protected $maxMinutes = 9;

    /**
     * Add times between itself
     * @param array $times
     */
    public function addTimes($times) {
        $sumMin = 0;
        $sumSec = 0;
        $sumMs = 0;

        foreach ($times as $time){
            $regex = preg_match($this->mk8TimeRegex, $time, $matches);

            if (!$regex){
                throw Mk8TimeException::timeNotMatchWithFormat($time);
            }

            $sumMin += $matches[1];
            $sumSec += $matches[2];
            $sumMs += $matches[3];
        }

        $ms = $sumMs % 1000;
        $sumSec += floor($sumMs/1000);

        $sec = $sumSec % 60;
        $sumMin += floor($sumSec/60);

        return $sumMin.':'.sprintf( "%02u", $sec).'.'.sprintf( "%03u", $ms);
    }
}
