<?php
/**
 * ETML
 * Author: Loïc Herzig
 * Date: 07.02.2019
 */

namespace App\Http\Middleware;

use Closure;
use Auth;

/**
 * Check if the user is admin for the route
 */
class CheckAdmin
{
    /**
     * Handle an incoming request and check if the user is admin.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->is_admin){
            return response()->json([
                'message' => 'Unauthorized action.'
            ], 403);
        }

        return $next($request);
    }
}
