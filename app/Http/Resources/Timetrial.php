<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Timetrial extends JsonResource
{
/**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'cc' => $this->cc,
            'track' => new Track($this->track),
            'character' => new Character($this->character),
            'body' => new Body($this->body),
            'tire' => new Tire($this->tire),
            'glider' => new Glider($this->glider),
            'user' => new User($this->user),
            //'country' => new Country($this->user->country),
            'time' => $this->time,
            'video' => $this->video,
            'splits' => $this->splits->map(function ($split){
                return [
                    'id' => $split->id,
                    'time' => $split->time
                ];
            }),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
