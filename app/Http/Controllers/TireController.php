<?php

namespace App\Http\Controllers;

use App\Models\Tire;
use Illuminate\Http\Request;

use App\Http\Resources\Tire as TireResource;

class TireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TireResource::collection(Tire::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tire  $tire
     * @return \Illuminate\Http\Response
     */
    public function show($tire)
    {
        return new TireResource( Tire::getInstanceOrFail($tire) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tire  $tire
     * @return \Illuminate\Http\Response
     */
    public function delete($tire)
    {
        Tire::getInstanceOrFail($tire)->delete();

        return response()->json(null, 204);
    }
}
