<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Traits\Mk8Time;

use App\Models\Timetrial;
use App\Models\User;
use App\Models\Country;
use App\Models\Split;
use App\Models\Track;
use App\Models\Character;
use App\Models\Body;
use App\Models\Tire;
use App\Models\Glider;

use App\Http\Resources\Timetrial as TimetrialResource;
use App\Http\Resources\GlobalCollection;

class TimetrialController extends Controller
{
    use Mk8Time;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Timetrial $timetrial)
    {
        $validatedData = $request->validate([
            'cc' => Rule::in(config('mk8dx.ccs')),
            'top' => 'integer',
            'bottom' => 'integer',
        ]);

        $timetrial = $timetrial->newQuery();

        if ($request->has('tracks')) {
            $tracks = collect($request->tracks)->map(function ($track) {
                return Track::getInstanceOrNull($track);
            });

            $timetrial->whereHas('track', function($query) use ($tracks){
                $query->whereIn('id', $tracks->pluck('id'));
            });
        }

        if ($request->has('characters')) {
            $characters = collect($request->characters)->map(function ($character) {
                return Character::getInstanceOrNull($character);
            });

            $timetrial->whereHas('character', function($query) use ($characters){
                $query->whereIn('id', $characters->pluck('id'));
            });
        }

        if ($request->has('bodies')) {
            $bodies = collect($request->bodies)->map(function ($body) {
                return Body::getInstanceOrNull($body);
            });

            $timetrial->whereHas('body', function($query) use ($bodies){
                $query->whereIn('id', $bodies->pluck('id'));
            });
        }

        if ($request->has('tires')) {
            $tires = collect($request->tires)->map(function ($tire) {
                return Tire::getInstanceOrNull($tire);
            });

            $timetrial->whereHas('tire', function($query) use ($tires){
                $query->whereIn('id', $tires->pluck('id'));
            });
        }

        if ($request->has('gliders')) {
            $gliders = collect($request->gliders)->map(function ($glider) {
                return Glider::getInstanceOrNull($glider);
            });

            $timetrial->whereHas('glider', function($query) use ($gliders){
                $query->whereIn('id', $gliders->pluck('id'));
            });
        }

        if ($request->has('cc')) {
            $timetrial->where('cc', $request->cc);
        }

        if ($request->has('countries')){
            $countries = collect($request->countries)->map(function ($country) {
                return Country::getInstanceOrNull($country);
            });

            $timetrial->whereHas('user.country', function($query) use ($countries){
                $query->whereIn('country_id', $countries->pluck('id'));
            });
        }

        if ($request->has('users')){
            $users = collect($request->users)->map(function ($user) {
                return User::getInstanceOrNull($user);
            });

            $timetrial->whereHas('user', function($query) use ($users){
                $query->whereIn('id', $users->pluck('id'));
            });
        }

        if ($request->has('discord_id')){
            $users = collect($request->discord_id)->map(function ($discord_id) {
                return User::where('discord_id', $discord_id)->first();
            });

            $timetrial->whereHas('user', function($query) use ($users){
                $query->whereIn('id', $users->pluck('id'));
            });
        }

        if ($request->has('top')){
            $timetrial->orderBy('time', 'asc')->take($request->top);
        }

        if ($request->has('bottom')){
            $timetrial->orderBy('time', 'desc')->take($request->bottom);
        }

        $timetrial->with('splits');

        return TimetrialResource::collection( $timetrial->get() );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'track' => 'required',
            'cc' => ['required', Rule::in(config('mk8dx.ccs'))],
            'time' => 'required|regex:'.$this->mk8TimeRegex,
            'splits' => 'nullable|array',
            'splits.*' => 'regex:'.$this->mk8TimeRegex,
            'character' => 'nullable',
            'body' => 'nullable',
            'tire' => 'nullable',
            'glider' => 'nullable',
            'video' => 'nullable|url',
            'player' => 'nullable'
        ]);

        // Check if admin want add to a player
        if ($request->player) {
            if (!$request->user()->is_admin) {
                return response()->json([
                    'message' => 'Unauthorized action.'
                ], 403);
            }

            $user = User::getInstanceOrFail($request->player);
        }
        else {
            $user = $request->user();
        }

        // Get resources needed and check if exists if needed
        $cc = $request->cc;
        $video = $request->video;
        $track = Track::getInstanceOrFail($request->track);
        $character = $request->character ? Character::getInstanceOrFail($request->character) : null;
        $body = $request->body ? Body::getInstanceOrFail($request->body) : null;
        $tire = $request->tire ? Tire::getInstanceOrFail($request->tire) : null;
        $glider = $request->glider ? Glider::getInstanceOrFail($request->glider) : null;
        $time = $request->time;
        $splits = $request->splits;

        if ($splits) {
            if (count($splits) < $track->laps){
                return response()->json([
                    'message' => 'One or many splits are missing.'
                ], 400);
            }
            else if (count($splits) > $track->laps){
                return response()->json([
                    'message' => 'There is too much splits.'
                ], 400);
            }

            // Check if splits = time
            if ($this->addTimes($splits) != $time){
                return response()->json([
                    'message' => 'Splits do not correspond to time.'
                ], 400);
            }
        }

        $timetrial = new Timetrial();
        $timetrial->time = $time;
        $timetrial->cc = $cc;
        $timetrial->video = $video;
        $timetrial->track()->associate($track);
        $timetrial->character()->associate($character);
        $timetrial->body()->associate($body);
        $timetrial->tire()->associate($tire);
        $timetrial->glider()->associate($glider);
        $timetrial->user()->associate($user);
        $timetrial->save();

        if ($splits) {
            foreach ($splits as $split_){
                $split = new Split();
                $split->time = $split_;
                $split->timetrial()->associate($timetrial);
                $split->save();
            }
        }

        return response([
            'message' => 'Timetrial successfully added!',
            'timetrial' => new TimetrialResource($timetrial)
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $timetrial
     * @return \Illuminate\Http\Response
     */
    public function show($timetrial)
    {
        return new TimetrialResource( Timetrial::getInstanceOrFail($timetrial) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Timetrial  $timetrial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Timetrial $timetrial)
    {
        if (!$request->user()->is_admin && $request->user()->id != $timetrial->user->id) {
            return response()->json([
                'message' => 'Unauthorized action.'
            ], 403);
        }

        $validatedData = $request->validate([
            'track' => 'nullable',
            'cc' => ['nullable', Rule::in(config('mk8dx.ccs'))],
            'time' => 'nullable|regex:'.$this->mk8TimeRegex,
            'splits' => 'nullable|array',
            'splits.*' => 'regex:'.$this->mk8TimeRegex,
            'character' => 'nullable',
            'body' => 'nullable',
            'tire' => 'nullable',
            'glider' => 'nullable',
            'video' => 'nullable|url'
        ]);

        // Get resources and check if exists if needed
        $track = $request->track ? Track::getInstanceOrFail($request->track) : $timetrial->track;
        $character = $request->character ? Character::getInstanceOrFail($request->character) : null;
        $body = $request->body ? Body::getInstanceOrFail($request->body) : null;
        $tire = $request->tire ? Tire::getInstanceOrFail($request->tire) : null;
        $glider = $request->glider ? Glider::getInstanceOrFail($request->glider) : null;
        $cc = $request->cc;
        $time = $request->time ? $request->time : $timetrial->time;
        $splits = $request->splits;
        $video = $request->video;

        if ($splits) {
            if (count($request->splits) < $track->laps){
                return response()->json([
                    'message' => 'One or many splits are missing.'
                ], 400);
            }
            else if (count($request->splits) > $track->laps){
                return response()->json([
                    'message' => 'There is too much splits.'
                ], 400);
            }

            // Check if splits = time
            if ($this->addTimes($splits) != $time){
                return response()->json([
                    'message' => 'Splits do not correspond to time.'
                ], 400);
            }
        }

        if ($cc) { $timetrial->cc = $cc; }
        if ($time) { $timetrial->time = $time; }
        if ($character) { $timetrial->character()->associate($character); }
        if ($body) { $timetrial->body()->associate($body); }
        if ($tire) { $timetrial->tire()->associate($tire); }
        if ($glider) { $timetrial->glider()->associate($glider); }
        if ($video) { $timetrial->video = $video; }
        $timetrial->updated_at = now();
        $timetrial->save();

        if ($splits) {
            if (count($timetrial->splits) == 0) {
                foreach ($splits as $split_){
                    $split = new Split();
                    $split->time = $split_;
                    $split->timetrial()->associate($timetrial);
                    $split->save();
                }
            }
            else {
                $i = 0;
                foreach ($timetrial->splits as $split){
                    $split->time = $splits[$i];
                    $split->save();
                    $i++;
                }
            }
        }

        return response()->json([
            'message' => 'Timetrial successfully updated!',
            'timetrial' => new TimetrialResource($timetrial)
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Timetrial  $timetrial
     * @return \Illuminate\Http\Response
     */
     public function delete(Request $request, Timetrial $timetrial)
     {
         if (!$request->user()->is_admin && $request->user()->id != $timetrial->user->id) {
             return response()->json([
                 'message' => 'Unauthorized action.'
             ], 403);
         }

         Timetrial::getInstanceOrFail($timetrial)->delete();

         return response()->json([
             'message' => 'Timetrial successfully deleted'
         ], 204);
     }
}
