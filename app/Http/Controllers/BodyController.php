<?php

namespace App\Http\Controllers;

use App\Models\Body;
use Illuminate\Http\Request;

use App\Http\Resources\Body as BodyResource;

class BodyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return BodyResource::collection(Body::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Body  $body
     * @return \Illuminate\Http\Response
     */
    public function show($body)
    {
        return new BodyResource( Body::getInstanceOrFail($body) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Body  $body
     * @return \Illuminate\Http\Response
     */
    public function delete($body)
    {
        Body::getInstanceOrFail($body)->delete();

        return response()->json(null, 204);
    }
}
