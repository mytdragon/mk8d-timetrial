<?php

namespace App\Http\Controllers;

use App\Models\Glider;
use Illuminate\Http\Request;

use App\Http\Resources\Glider as GliderResource;

class GliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return GliderResource::collection(Glider::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Glider  $glider
     * @return \Illuminate\Http\Response
     */
    public function show($glider)
    {
        return new GliderResource( Glider::getInstanceOrFail($glider) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Glider  $glider
     * @return \Illuminate\Http\Response
     */
    public function delete($glider)
    {
        Glider::getInstanceOrFail($glider)->delete();

        return response()->json(null, 204);
    }
}
