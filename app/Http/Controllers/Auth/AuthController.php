<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\SignupActivate;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Country;

use App\Http\Resources\User as UserResource;
use App\Http\Resources\GlobalCollection;

class AuthController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] pseudo
     * @param  [string] country
     * @return [string] message
     */
    public function signup(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'pseudo' => 'required|string|between:3,255|unique:users',
            'country' => 'required'
        ]);

        $country = Country::getInstanceOrFail($request->country);

        $user = new User();
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->pseudo = $request->pseudo;
        $user->activation_token = str_random(60);
        $user->country()->associate($country);
        $user->save();

        $user->notify(new SignupActivate($user));

        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }

    /**
     * Create user using the discord id
     *
     * @param  [string] discord_id
     * @param  [string] pseudo
     * @param  [string] country
     * @return [string] message
     */
    public function signupViaDiscord(Request $request)
    {
        $request->validate([
            'discord_id' => 'required|string',
            'pseudo' => 'required|string|between:3,255',
            'country' => 'required'
        ]);

        $country = Country::getInstanceOrFail($request->country);

        if (User::where('discord_id', $request->discord_id)->exists()){
            return response()->json([
                //'message' => 'A user with the discord_id `'.$request->discord_id.'` is already registered.'
                'message' => 'You are already registered.'
            ], 409);
        }

        if (User::where('pseudo', $request->pseudo)->exists()){
            return response()->json([
                'message' => 'A user with the pseudo `'.$request->pseudo.'` is already registered.'
            ], 409);
        }

        $user = new User();
        $user->discord_id = $request->discord_id;
        $user->pseudo = $request->pseudo;
        $user->country()->associate($country);
        $user->save();

        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|min:8|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);

        if(!Auth::attempt($credentials)){
            return response()->json([
                'error' => 'InvalidCredentials',
                'dev' => 'Credentials does not match.',
                'user' => 'Credentials does not match.'
            ], 401);

        }

        $user = $request->user();

        if (!$user->active){
            return response()->json([
                'error' => 'UserNotActivated',
                'dev' => 'User not activated.',
                'user' => 'Please verify your email address.'
            ], 401);
        }

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me){
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        else {
            $token->expires_at = Carbon::now()->addHours(1);
        }
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    public function signupActivate($token)
    {
        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return response()->json([
                'message' => 'This activation token is invalid.'
            ], 404);
        }
        $user->active = true;
        $user->activation_token = '';
        $user->email_verified_at = Carbon::now();
        $user->save();

        return response()->json([
            'message' => 'Your account has been activated.'
        ], 200);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out.'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return new UserResource( $request->user() );
    }

    /**
     * Update the authenticated User
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'pseudo' => 'string|between:3,255',
            'discord_id' => 'integer',
            'switch_fc' => 'string|size:14|regex:/^([0-9]{4})-([0-9]{4})-([0-9]{4})$/'
        ]);

        $request->user()->update($request->all());
        $request->user()->save();

        return new UserResource( $request->user() );
    }
}
