<?php

namespace App\Http\Controllers;

use App\Models\Track;
use Illuminate\Http\Request;

use App\Http\Resources\Track as TrackResource;

class TrackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TrackResource::collection(Track::all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function show($track)
    {
        return new TrackResource( Track::getInstanceOrFail($track) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function delete($track)
    {
        Track::getInstanceOrFail($track)->delete();

        return response()->json(null, 204);
    }
}
