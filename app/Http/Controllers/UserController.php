<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Resources\User as UserResource;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {
        $validatedData = $request->validate([
            'pseudo' => 'string|between:3,255',
            'discord_id' => 'integer'
        ]);

        $user = $user->newQuery();

        if ($request->has('pseudo')) {
            $user->where('pseudo', $request->pseudo);
        }

        if ($request->has('discord_id')) {
            $user->where('discord_id', $request->discord_id);
        }

        return UserResource::collection($user->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($user)
    {
        return new UserResource( User::getInstanceOrFail($user) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $timetrial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validatedData = $request->validate([
            'pseudo' => 'string|between:3,255',
            'discord_id' => 'integer',
            'switch_fc' => 'string|size:14|regex:/^([0-9]{4})-([0-9]{4})-([0-9]{4})$/'
        ]);

        $user->update($request->all());
        $user->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function delete($user)
    {
        User::getInstanceOrFail($user)->delete();

        return response()->json(null, 204);
    }
}
