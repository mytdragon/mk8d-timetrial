<?php
namespace App\Exceptions;

use InvalidArgumentException;

class ModelNotFoundException extends InvalidArgumentException
{
    public static function global(string $model)
    {
        return new static("{$model} could not be found.");
    }

    public static function id(string $model, string $id)
    {
        return new static("{$model} with id `{$id}` could not be found.");
    }

    public static function name(string $model, string $name)
    {
        return new static("{$model} with name `{$name}` could not be found.");
    }

    public static function pseudo(string $model, string $pseudo)
    {
        return new static("{$model} with pseudo `{$pseudo}` could not be found.");
    }

    public static function code(string $model, string $code)
    {
        return new static("{$model} with code `{$code}` could not be found.");
    }
}
