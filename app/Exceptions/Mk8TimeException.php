<?php
namespace App\Exceptions;

use InvalidArgumentException;

class Mk8TimeException extends InvalidArgumentException
{
    public static function timeNotMatchWithFormat(string $time)
    {
        return new static("Time `{$time}`'s format is invalid (`0:00.000` required).");
    }

    public static function timeTooLong(string $time, $maxMinutes)
    {
        return new static("Time `{$time}`' is higher than {$maxMinutes} minutes and 59.999 seconds.");
    }
}
