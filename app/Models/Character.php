<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\ModelNotFoundException;

class Character extends Model
{
    protected $table = 'characters';
    public $timestamps = false;
    protected $fillable = ['name'];
    protected $guarded = ['id'];
    public function timetrials(){ return $this->hasMany('App\Models\Timetrial'); }

    /**
     * Find by its id
     * @param  int $id
     * @return Character
     */
    public static function findByIdOrFail(int $id) : Character {
        $character = Character::where('id', $id)->first();

        if (!$character) { throw ModelNotFoundException::id('Character', $id); }

        return $character;
    }

    /**
     * Find by its id or null
     * @param  int $id
     * @return Character|null
     */
    public static function findByIdOrNull(int $id) : ?Character {
        return Character::where('id', $id)->first();
    }

    /**
     * Find by its name
     * @param  string $name
     * @return Character
     */
    public static function findByNameOrFail(string $name) : Character {
        $character = Character::where('name', $name)->first();

        if (!$character) { throw ModelNotFoundException::name('Character', $name); }

        return $character;
    }

    /**
     * Find by its name or null
     * @param  string $name
     * @return Character|null
     */
    public static function findByNameOrNull(string $name) : ?Character {
        return Character::where('name', $name)->first();
    }

    /**
     * Get instance of the model
     * @param  string|integer|Character $character name, id or Character instance
     * @return Character
     */
    public static function getInstanceOrFail($character) : Character {
        if (is_numeric($character)){
            return Character::findByIdOrFail($character);
        }

        if (is_string($character)){
            return Character::findByNameOrFail($character);
        }

        if (!$character instanceof Character){
            throw ModelNotFoundException::global('Character');
        }

        return $character;
    }

    /**
     * Get instance of the model or null
     * @param  string|integer|Character $character name, id or Character instance
     * @return Character|null
     */
    public static function getInstanceOrNull($character) : ?Character {
        if (is_numeric($character)){
            return Character::findByIdOrNull($character);
        }

        if (is_string($character)){
            return Character::findByNameOrNull($character);
        }

        if ($character instanceof Character){
            return $character;
        }

        return null;
    }
}
