<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\ModelNotFoundException;

class Tire extends Model
{
    protected $table = 'tires';
    public $timestamps = false;
    protected $fillable = ['name'];
    protected $guarded = ['id'];
    public function timetrials(){ return $this->hasMany('App\Models\Timetrial'); }

    /**
     * Find by its id
     * @param  int $id
     * @return Tire
     */
    public static function findByIdOrFail(int $id) : Tire {
        $tire = Tire::where('id', $id)->first();

        if (!$tire) { throw ModelNotFoundException::id('Tire', $id); }

        return $tire;
    }

    /**
     * Find by its id or null
     * @param  int $id
     * @return Tire|null
     */
    public static function findByIdOrNull(int $id) : ?Tire {
        return Tire::where('id', $id)->first();
    }

    /**
     * Find by its name
     * @param  string $name
     * @return Tire
     */
    public static function findByNameOrFail(string $name) : Tire {
        $tire = Tire::where('name', $name)->first();

        if (!$tire) { throw ModelNotFoundException::name('Tire', $name); }

        return $tire;
    }

    /**
     * Find by its name or null
     * @param  string $name
     * @return Tire|null
     */
    public static function findByNameOrNull(string $name) : ?Tire {
        return Tire::where('name', $name)->first();
    }

    /**
     * Get instance of the model
     * @param  string|integer|Tire $tire name, id or Tire instance
     * @return Tire
     */
    public static function getInstanceOrFail($tire) : Tire {
        if (is_numeric($tire)){
            return Tire::findByIdOrFail($tire);
        }

        if (is_string($tire)){
            return Tire::findByNameOrFail($tire);
        }

        if (!$tire instanceof Tire){
            throw ModelNotFoundException::global('Tire');
        }

        return $tire;
    }

    /**
     * Get instance of the model or null
     * @param  string|integer|Tire $tire name, id or Tire instance
     * @return Tire|null
     */
    public static function getInstanceOrNull($tire) : ?Tire {
        if (is_numeric($tire)){
            return Tire::findByIdOrNull($tire);
        }

        if (is_string($tire)){
            return Tire::findByNameOrNull($tire);
        }

        if ($tire instanceof Tire){
            return $tire;
        }

        return null;
    }
}
