<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    protected $table = 'users';
    protected $fillable = ['email', 'password', 'pseudo', 'discord_id', 'switch_fc', 'active', 'activation_token', 'is_admin'];
    protected $hidden = ['password', 'remember_token', 'activation_token', 'email_verified_at'];
    protected $casts = ['email_verified_at' => 'datetime'];
    public function timetrials(){ return $this->hasMany('App\Models\Timetrial'); }
    public function country(){ return $this->belongsTo('App\Models\Country'); }

    /**
     * Find by its id
     * @param  int $id
     * @return User
     */
    public static function findByIdOrFail(int $id) : User {
        $user = User::where('id', $id)->first();

        if (!$user) { throw ModelNotFoundException::id('User', $id); }

        return $user;
    }

    /**
     * Find by its id or null
     * @param  int $id
     * @return User|null
     */
    public static function findByIdOrNull(int $id) : ?User {
        return User::where('id', $id)->first();
    }

    /**
     * Find by its pseudo
     * @param  string $pseudo
     * @return User
     */
    public static function findByPseudoOrFail(string $pseudo) : User {
        $user = User::where('pseudo', $pseudo)->first();

        if (!$user) { throw ModelNotFoundException::pseudo('User', $pseudo); }

        return $user;
    }

    /**
     * Find by its pseudo or null
     * @param  string $pseudo
     * @return User|null
     */
    public static function findByPseudoOrNull(string $pseudo) : ?User {
        return User::where('pseudo', $pseudo)->first();
    }

    /**
     * Get instance of the model
     * @param  string|integer|User $user pseudo, id or User instance
     * @return User
     */
    public static function getInstanceOrFail($user) : User {
        if (is_numeric($user)){
            return User::findByIdOrFail($user);
        }

        if (is_string($user)){
            return User::findByPseudoOrFail($user);
        }

        if (!$user instanceof User){
            throw ModelNotFoundException::global('User');
        }

        return $user;
    }

    /**
     * Get instance of the model or null
     * @param  string|integer|User $user pseudo, id or User instance
     * @return User|null
     */
    public static function getInstanceOrNull($user) : ?User {
        if (is_numeric($user)){
            return User::findByIdOrNull($user);
        }

        if (is_string($user)){
            return User::findByPseudoOrNull($user);
        }

        if ($user instanceof User){
            return $user;
        }

        return null;
    }
}
