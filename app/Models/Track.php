<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\ModelNotFoundException;

class Track extends Model
{
    protected $table = 'tracks';
    public $timestamps = false;
    protected $fillable = ['name', 'code', 'laps'];
    protected $guarded = ['id'];
    public function timetrials(){ return $this->hasMany('App\Models\Timetrial'); }

    /**
     * Find by its id
     * @param  int $id
     * @return Track
     */
    public static function findByIdOrFail(int $id) : Track {
        $track = Track::where('id', $id)->first();

        if (!$track) { throw ModelNotFoundException::id('Track', $id); }

        return $track;
    }

    /**
     * Find by its id or null
     * @param  int $id
     * @return Track|null
     */
    public static function findByIdOrNull(int $id) : ?Track {
        return Track::where('id', $id)->first();
    }

    /**
     * Find by its name
     * @param  string $name
     * @return Track
     */
    public static function findByNameOrFail(string $name) : Track {
        $track = Track::where('name', $name)->first();

        if (!$track) { throw ModelNotFoundException::name('Track', $name); }

        return $track;
    }

    /**
     * Find by its name or null
     * @param  string $name
     * @return Track|null
     */
    public static function findByNameOrNull(string $name) : ?Track {
        return Track::where('name', $name)->first();
    }

    /**
     * Find by its code
     * @param  string $code
     * @return Track
     */
    public static function findByCodeOrFail(string $code) : Track {
        $track = Track::where('code', $code)->first();

        if (!$track) { throw ModelNotFoundException::code('Track', $code); }

        return $track;
    }

    /**
     * Find by its code or null
     * @param  string $code
     * @return Track|null
     */
    public static function findByCodeOrNull(string $code) : ?Track {
        return Track::where('code', $code)->first();
    }

    /**
     * Get instance of the model
     * @param  string|integer|Track $track name, id or Track instance
     * @return Track
     */
    public static function getInstanceOrFail($track) : Track {
        if (is_numeric($track)){
            return Track::findByIdOrFail($track);
        }

        if (is_string($track) && mb_strlen($track, 'utf8') <= 4){
            return Track::findByCodeOrFail($track);
        }

        if (is_string($track)){
            return Track::findByNameOrFail($track);
        }

        if (!$track instanceof Track){
            throw ModelNotFoundException::global('Track');
        }

        return $track;
    }

    /**
     * Get instance of the model or null
     * @param  string|integer|Track $track name, id or Track instance
     * @return Track|null
     */
    public static function getInstanceOrNull($track) : ?Track {
        if (is_numeric($track)){
            return Track::findByIdOrNull($track);
        }

        if (is_string($track) && mb_strlen($track, 'utf8') <= 4){
            return Track::findByCodeOrNull($track);
        }

        if (is_string($track)){
            return Track::findByNameOrNull($track);
        }

        if ($track instanceof Track){
            return $track;
        }

        return null;
    }
}
