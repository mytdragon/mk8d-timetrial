<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\ModelNotFoundException;

class Glider extends Model
{
    protected $table = 'gliders';
    public $timestamps = false;
    protected $fillable = ['name'];
    protected $guarded = ['id'];
    public function timetrials(){ return $this->hasMany('App\Models\Timetrial'); }

    /**
     * Find by its id
     * @param  int $id
     * @return Glider
     */
    public static function findByIdOrFail(int $id) : Glider {
        $glider = Glider::where('id', $id)->first();

        if (!$glider) { throw ModelNotFoundException::id('Glider', $id); }

        return $glider;
    }

    /**
     * Find by its id or null
     * @param  integer $id
     * @return Glider|null
     */
    public static function findByIdOrNull(integer $id) : ?Glider {
        return Glider::where('id', $id)->first();
    }


    /**
     * Find by its name
     * @param  string $name
     * @return Glider
     */
    public static function findByNameOrFail(string $name) : Glider{
        $glider = Glider::where('name', $name)->first();

        if (!$glider) { throw ModelNotFoundException::name('Glider', $name); }

        return $glider;
    }

    /**
     * Find by its name or null
     * @param  string $name
     * @return Glider|null
     */
    public static function findByNameOrNull(string $name) : ?Glider {
        return Glider::where('name', $name)->first();
    }

    /**
     * Get instance of the model
     * @param  string|integer|Glider $glider name, id or Glider instance
     * @return Glider
     */
    public static function getInstanceOrFail($glider) : Glider {
        if (is_numeric($glider)){
            return Glider::findByIdOrFail($glider);
        }

        if (is_string($glider)){
            return Glider::findByNameOrFail($glider);
        }

        if (!$glider instanceof Glider){
            throw ModelNotFoundException::global('Glider');
        }

        return $glider;
    }

    /**
     * Get instance of the model or null
     * @param  string|integer|Glider $glider name, id or Glider instance
     * @return Glider|null
     */
    public static function getInstanceOrNull($glider) : ?Glider {
        if (is_numeric($glider)){
            return Glider::findByIdOrNull($glider);
        }

        if (is_string($glider)){
            return Glider::findByNameOrNull($glider);
        }

        if ($glider instanceof Glider){
            return $glider;
        }

        return null;
    }
}
