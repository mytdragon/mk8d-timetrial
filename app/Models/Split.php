<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Split extends Model
{
    protected $table = 'splits';
    protected $fillable = ['time'];
    protected $guarded = ['id'];
    public $timestamps = false;
    public function timetrial(){ return $this->belongsTo('App\Models\Timetrial'); }
}
