<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\ModelNotFoundException;

class Country extends Model
{
    protected $table = 'countries';
    public $timestamps = false;
    protected $fillable = ['name', 'code'];
    protected $guarded = ['id'];
    public function users(){ return $this->hasMany('App\Models\User'); }

    /**
     * Find by its id
     * @param  int $id
     * @return Country
     */
    public static function findByIdOrFail(int $id) : Country {
        $country = Country::where('id', $id)->first();

        if (!$country) { throw ModelNotFoundException::id('Country', $id); }

        return $country;
    }

    /**
     * Find by its id or null
     * @param  int $id
     * @return Country|null
     */
    public static function findByIdOrNull(int $id) : ?Country {
        return Country::where('id', $id)->first();
    }

    /**
     * Find by its name
     * @param  string $name
     * @return Country
     */
    public static function findByNameOrFail(string $name) : Country {
        $country = Country::where('name', $name)->first();

        if (!$country) { throw ModelNotFoundException::name('Country', $name); }

        return $country;
    }

    /**
     * Find by its name or null
     * @param  string $name
     * @return Country|null
     */
    public static function findByNameOrNull(string $name) : ?Country {
        return Country::where('name', $name)->first();
    }

    /**
     * Find by its code
     * @param  string $code
     * @return Country
     */
    public static function findByCodeOrFail(string $code) : Country {
        $country = Country::where('code', $code)->first();

        if (!$country) { throw ModelNotFoundException::code('Country', $code); }

        return $country;
    }

    /**
     * Find by its code or null
     * @param  string $code
     * @return Country|null
     */
    public static function findByCodeOrNull(string $code) : ?Country {
        return Country::where('code', $code)->first();
    }

    /**
     * Get instance of the model
     * @param  string|integer|Country $country name, id or Country instance
     * @return Country
     */
    public static function getInstanceOrFail($country) : Country {
        if (is_numeric($country)){
            return Country::findByIdOrFail($country);
        }

        if (is_string($country) && mb_strlen($country, 'utf8') == 2){
            return Country::findByCodeOrFail($country);
        }

        if (is_string($country)){
            return Country::findByNameOrFail($country);
        }

        if (!$country instanceof Country){
            throw ModelNotFoundException::global('Country');
        }

        return $country;
    }

    /**
     * Get instance of the model or null
     * @param  string|integer|Country $country name, id or Country instance
     * @return Country|null
     */
    public static function getInstanceOrNull($country) : ?Country {
        if (is_numeric($country)){
            return Country::findByIdOrNull($country);
        }

        if (is_string($country) && mb_strlen($country, 'utf8') <= 4){
            return Country::findByCodeOrNull($country);
        }

        if (is_string($country)){
            return Country::findByNameOrNull($country);
        }

        if ($country instanceof Country){
            return $country;
        }

        return null;
    }
}
