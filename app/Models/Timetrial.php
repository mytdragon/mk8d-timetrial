<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\ModelNotFoundException;

class Timetrial extends Model
{
    protected $table = 'timetrials';
    protected $fillable = ['time', 'cc'];
    protected $guarded = ['id'];
    public function splits(){ return $this->hasMany('App\Models\Split'); }
    public function track(){ return $this->belongsTo('App\Models\Track'); }
    public function character(){ return $this->belongsTo('App\Models\Character'); }
    public function body(){ return $this->belongsTo('App\Models\Body'); }
    public function tire(){ return $this->belongsTo('App\Models\Tire'); }
    public function glider(){ return $this->belongsTo('App\Models\Glider'); }
    public function user(){ return $this->belongsTo('App\Models\User'); }

    /**
     * Find by its id
     * @param  int $id
     * @return Timetrial
     */
    public static function findByIdOrFail(int $id) : Timetrial {
        $timetrial = Timetrial::where('id', $id)->first();

        if (!$timetrial) { throw ModelNotFoundException::id('Timetrial', $id); }

        return $timetrial;
    }

    /**
     * Find by its id or null
     * @param  integer $id
     * @return Timetrial|null
     */
    public static function findByIdOrNull(integer $id) : ?Timetrial {
        return Timetrial::where('id', $id)->first();
    }


    /**
     * Get instance of the model
     * @param  string|integer|Timetrial $timetrial name, id or Timetrial instance
     * @return Timetrial
     */
    public static function getInstanceOrFail($timetrial) : Timetrial {
        if (is_numeric($timetrial)){
            return Timetrial::findByIdOrFail($timetrial);
        }

        if (!$timetrial instanceof Timetrial){
            throw ModelNotFoundException::global('Timetrial');
        }

        return $timetrial;
    }

    /**
     * Get instance of the model or null
     * @param  string|integer|Timetrial $timetrial name, id or Timetrial instance
     * @return Timetrial|null
     */
    public static function getInstanceOrNull($timetrial) : ?Timetrial {
        if (is_numeric($timetrial)){
            return Timetrial::find($timetrial);
        }

        if ($timetrial instanceof Timetrial){
            return $timetrial;
        }

        return null;
    }
}
