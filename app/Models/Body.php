<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\ModelNotFoundException;

class Body extends Model
{
    protected $table = 'bodies';
    public $timestamps = false;
    protected $fillable = ['name'];
    protected $guarded = ['id'];
    public function timetrials(){ return $this->hasMany('App\Models\Timetrial'); }

    /**
     * Find by its id
     * @param  int $id
     * @return Body
     */
    public static function findByIdOrFail(int $id) : Body {
        $body = Body::where('id', $id)->first();

        if (!$body) { throw ModelNotFoundException::id('Body', $id); }

        return $body;
    }

    /**
     * Find by its id or null
     * @param  int $id
     * @return Body|null
     */
    public static function findByIdOrNull(int $id) : ?Body {
        return Body::where('id', $id)->first();
    }

    /**
     * Find by its name
     * @param  string $name
     * @return Body
     */
    public static function findByNameOrFail(string $name) : Body {
        $body = Body::where('name', $name)->first();

        if (!$body) { throw ModelNotFoundException::name('Body', $name); }

        return $body;
    }

    /**
     * Find by its name or null
     * @param  string $name
     * @return Body|null
     */
    public static function findByNameOrNull(string $name) : ?Body{
        return Body::where('name', $name)->first();
    }

    /**
     * Get instance of the model
     * @param  string|integer|Body $body name, id or Body instance
     * @return Body
     */
    public static function getInstanceOrFail($body) : Body {
        if (is_numeric($body)){
            return Body::findByIdOrFail($body);
        }

        if (is_string($body)){
            return Body::findByNameOrFail($body);
        }

        if (!$body instanceof Body){
            throw ModelNotFoundException::global('Body');
        }

        return $body;
    }

    /**
     * Get instance of the model or null
     * @param  string|integer|Body $body name, id or Body instance
     * @return Body|null
     */
    public static function getInstanceOrNull($body) : ?Body {
        if (is_numeric($body)){
            return Body::findByIdOrNull($body);
        }

        if (is_string($body)){
            return Body::findByNameOrNull($body);
        }

        if ($body instanceof Body){
            return $body;
        }

        return null;
    }
}
