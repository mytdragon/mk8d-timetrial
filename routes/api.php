<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
    Route::post('login', 'AuthController@login')->name('login');
    Route::post('signup', 'AuthController@signup')->name('signup');
    Route::get('signup/activate/{token}', 'AuthController@signupActivate')->name('activateUser');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout')->name('logout');
        Route::group(['prefix' => 'user'], function () {
            Route::get('/', 'AuthController@user')->name('getAuthenticatedUser');
            Route::put('/update', 'AuthController@update')->name('updateAuthenticatedUser');
        });

        Route::group(['middleware' => 'checkAdmin'], function () {
            Route::post('signup/discord', 'AuthController@signupViaDiscord')->name('signupViaDiscord');
        });
    });

    Route::group(['prefix' => 'password'], function() {
        Route::post('create', 'PasswordResetController@create')->name('createPasswordReset');
        Route::get('find/{token}', 'PasswordResetController@find')->name('findPasswordReset');
        Route::patch('reset', 'PasswordResetController@reset')->name('resetPassword');
    });
});

Route::group(['prefix' => 'users'], function () {
    Route::get('/', 'UserController@index')->name('getUsers');
    Route::get('{user}', 'UserController@show')->name('showUser');
});

Route::get('countries', 'CountryController@index')->name('getCountries');
Route::get('countries/{country}', 'CountryController@show')->name('showCountry');

Route::get('characters', 'CharacterController@index')->name('getCharacters');
Route::get('characters/{character}', 'CharacterController@show')->name('showCharacter');

Route::get('bodies', 'BodyController@index')->name('getBodies');
Route::get('bodies/{body}', 'BodyController@show')->name('showBody');

Route::get('tires', 'TireController@index')->name('getTires');
Route::get('tires/{tire}', 'TireController@show')->name('showTire');

Route::get('gliders', 'GliderController@index')->name('getGliders');
Route::get('gliders/{glider}', 'GliderController@show')->name('showGlider');

Route::get('tracks', 'TrackController@index')->name('getTracks');
Route::get('tracks/{track}', 'TrackController@show')->name('showTrack');

Route::group(['prefix' => 'timetrials'], function () {
    Route::get('/', 'TimetrialController@index')->name('getTimetrials');
    Route::get('{timetrial}', 'TimetrialController@show')->name('showTimetrial');
    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('/', 'TimetrialController@store')->name('createTimetrial');
        Route::put('{timetrial}', 'TimetrialController@update')->name('updateTimetrial');
        Route::delete('{timetrial}', 'TimetrialController@delete')->name('deleteTimetrial');
    });
});

Route::group(['middleware' => ['auth:api', 'checkAdmin']], function () {
    Route::delete('countries/{country}', 'CountryController@delete')->name('deleteCountry');

    Route::put('users/{user}', 'UserController@update')->name('updateUser');
    Route::delete('users/{user}', 'UserController@delete')->name('deleteUser');

    Route::delete('tracks/{track}', 'TrackController@delete')->name('deleteTrack');

    Route::delete('characters/{characterk}', 'CharacterController@delete')->name('deleteCharacter');

    Route::delete('bodies/{body}', 'BodyController@delete')->name('deleteBody');

    Route::delete('tires/{tire}', 'TireController@delete')->name('deleteTire');

    Route::delete('gliders/{glider}', 'GliderController@delete')->name('deleteGlider');

    Route::delete('timetrials/{timetrial}', 'TimetrialController@delete')->name('deleteTimetrial');
});
