<?php
return [
  0 =>
  [
    'Code' => 'AF',
    'Name' => 'Afghanistan',
  ],
  1 =>
  [
    'Code' => 'AX',
    'Name' => 'Åland Islands',
  ],
  2 =>
  [
    'Code' => 'AL',
    'Name' => 'Albania',
  ],
  3 =>
  [
    'Code' => 'DZ',
    'Name' => 'Algeria',
  ],
  4 =>
  [
    'Code' => 'AS',
    'Name' => 'American Samoa',
  ],
  5 =>
  [
    'Code' => 'AD',
    'Name' => 'Andorra',
  ],
  6 =>
  [
    'Code' => 'AO',
    'Name' => 'Angola',
  ],
  7 =>
  [
    'Code' => 'AI',
    'Name' => 'Anguilla',
  ],
  8 =>
  [
    'Code' => 'AQ',
    'Name' => 'Antarctica',
  ],
  9 =>
  [
    'Code' => 'AG',
    'Name' => 'Antigua and Barbuda',
  ],
  10 =>
  [
    'Code' => 'AR',
    'Name' => 'Argentina',
  ],
  11 =>
  [
    'Code' => 'AM',
    'Name' => 'Armenia',
  ],
  12 =>
  [
    'Code' => 'AW',
    'Name' => 'Aruba',
  ],
  13 =>
  [
    'Code' => 'AU',
    'Name' => 'Australia',
  ],
  14 =>
  [
    'Code' => 'AT',
    'Name' => 'Austria',
  ],
  15 =>
  [
    'Code' => 'AZ',
    'Name' => 'Azerbaijan',
  ],
  16 =>
  [
    'Code' => 'BS',
    'Name' => 'Bahamas',
  ],
  17 =>
  [
    'Code' => 'BH',
    'Name' => 'Bahrain',
  ],
  18 =>
  [
    'Code' => 'BD',
    'Name' => 'Bangladesh',
  ],
  19 =>
  [
    'Code' => 'BB',
    'Name' => 'Barbados',
  ],
  20 =>
  [
    'Code' => 'BY',
    'Name' => 'Belarus',
  ],
  21 =>
  [
    'Code' => 'BE',
    'Name' => 'Belgium',
  ],
  22 =>
  [
    'Code' => 'BZ',
    'Name' => 'Belize',
  ],
  23 =>
  [
    'Code' => 'BJ',
    'Name' => 'Benin',
  ],
  24 =>
  [
    'Code' => 'BM',
    'Name' => 'Bermuda',
  ],
  25 =>
  [
    'Code' => 'BT',
    'Name' => 'Bhutan',
  ],
  26 =>
  [
    'Code' => 'BO',
    'Name' => 'Bolivia, Plurinational State of',
  ],
  27 =>
  [
    'Code' => 'BQ',
    'Name' => 'Bonaire, Sint Eustatius and Saba',
  ],
  28 =>
  [
    'Code' => 'BA',
    'Name' => 'Bosnia and Herzegovina',
  ],
  29 =>
  [
    'Code' => 'BW',
    'Name' => 'Botswana',
  ],
  30 =>
  [
    'Code' => 'BV',
    'Name' => 'Bouvet Island',
  ],
  31 =>
  [
    'Code' => 'BR',
    'Name' => 'Brazil',
  ],
  32 =>
  [
    'Code' => 'IO',
    'Name' => 'British Indian Ocean Territory',
  ],
  33 =>
  [
    'Code' => 'BN',
    'Name' => 'Brunei Darussalam',
  ],
  34 =>
  [
    'Code' => 'BG',
    'Name' => 'Bulgaria',
  ],
  35 =>
  [
    'Code' => 'BF',
    'Name' => 'Burkina Faso',
  ],
  36 =>
  [
    'Code' => 'BI',
    'Name' => 'Burundi',
  ],
  37 =>
  [
    'Code' => 'KH',
    'Name' => 'Cambodia',
  ],
  38 =>
  [
    'Code' => 'CM',
    'Name' => 'Cameroon',
  ],
  39 =>
  [
    'Code' => 'CA',
    'Name' => 'Canada',
  ],
  40 =>
  [
    'Code' => 'CV',
    'Name' => 'Cape Verde',
  ],
  41 =>
  [
    'Code' => 'KY',
    'Name' => 'Cayman Islands',
  ],
  42 =>
  [
    'Code' => 'CF',
    'Name' => 'Central African Republic',
  ],
  43 =>
  [
    'Code' => 'TD',
    'Name' => 'Chad',
  ],
  44 =>
  [
    'Code' => 'CL',
    'Name' => 'Chile',
  ],
  45 =>
  [
    'Code' => 'CN',
    'Name' => 'China',
  ],
  46 =>
  [
    'Code' => 'CX',
    'Name' => 'Christmas Island',
  ],
  47 =>
  [
    'Code' => 'CC',
    'Name' => 'Cocos (Keeling] Islands',
  ],
  48 =>
  [
    'Code' => 'CO',
    'Name' => 'Colombia',
  ],
  49 =>
  [
    'Code' => 'KM',
    'Name' => 'Comoros',
  ],
  50 =>
  [
    'Code' => 'CG',
    'Name' => 'Congo',
  ],
  51 =>
  [
    'Code' => 'CD',
    'Name' => 'Congo, the Democratic Republic of the',
  ],
  52 =>
  [
    'Code' => 'CK',
    'Name' => 'Cook Islands',
  ],
  53 =>
  [
    'Code' => 'CR',
    'Name' => 'Costa Rica',
  ],
  54 =>
  [
    'Code' => 'CI',
    'Name' => 'Côte d\'Ivoire',
  ],
  55 =>
  [
    'Code' => 'HR',
    'Name' => 'Croatia',
  ],
  56 =>
  [
    'Code' => 'CU',
    'Name' => 'Cuba',
  ],
  57 =>
  [
    'Code' => 'CW',
    'Name' => 'Curaçao',
  ],
  58 =>
  [
    'Code' => 'CY',
    'Name' => 'Cyprus',
  ],
  59 =>
  [
    'Code' => 'CZ',
    'Name' => 'Czech Republic',
  ],
  60 =>
  [
    'Code' => 'DK',
    'Name' => 'Denmark',
  ],
  61 =>
  [
    'Code' => 'DJ',
    'Name' => 'Djibouti',
  ],
  62 =>
  [
    'Code' => 'DM',
    'Name' => 'Dominica',
  ],
  63 =>
  [
    'Code' => 'DO',
    'Name' => 'Dominican Republic',
  ],
  64 =>
  [
    'Code' => 'EC',
    'Name' => 'Ecuador',
  ],
  65 =>
  [
    'Code' => 'EG',
    'Name' => 'Egypt',
  ],
  66 =>
  [
    'Code' => 'SV',
    'Name' => 'El Salvador',
  ],
  67 =>
  [
    'Code' => 'GQ',
    'Name' => 'Equatorial Guinea',
  ],
  68 =>
  [
    'Code' => 'ER',
    'Name' => 'Eritrea',
  ],
  69 =>
  [
    'Code' => 'EE',
    'Name' => 'Estonia',
  ],
  70 =>
  [
    'Code' => 'ET',
    'Name' => 'Ethiopia',
  ],
  71 =>
  [
    'Code' => 'FK',
    'Name' => 'Falkland Islands (Malvinas]',
  ],
  72 =>
  [
    'Code' => 'FO',
    'Name' => 'Faroe Islands',
  ],
  73 =>
  [
    'Code' => 'FJ',
    'Name' => 'Fiji',
  ],
  74 =>
  [
    'Code' => 'FI',
    'Name' => 'Finland',
  ],
  75 =>
  [
    'Code' => 'FR',
    'Name' => 'France',
  ],
  76 =>
  [
    'Code' => 'GF',
    'Name' => 'French Guiana',
  ],
  77 =>
  [
    'Code' => 'PF',
    'Name' => 'French Polynesia',
  ],
  78 =>
  [
    'Code' => 'TF',
    'Name' => 'French Southern Territories',
  ],
  79 =>
  [
    'Code' => 'GA',
    'Name' => 'Gabon',
  ],
  80 =>
  [
    'Code' => 'GM',
    'Name' => 'Gambia',
  ],
  81 =>
  [
    'Code' => 'GE',
    'Name' => 'Georgia',
  ],
  82 =>
  [
    'Code' => 'DE',
    'Name' => 'Germany',
  ],
  83 =>
  [
    'Code' => 'GH',
    'Name' => 'Ghana',
  ],
  84 =>
  [
    'Code' => 'GI',
    'Name' => 'Gibraltar',
  ],
  85 =>
  [
    'Code' => 'GR',
    'Name' => 'Greece',
  ],
  86 =>
  [
    'Code' => 'GL',
    'Name' => 'Greenland',
  ],
  87 =>
  [
    'Code' => 'GD',
    'Name' => 'Grenada',
  ],
  88 =>
  [
    'Code' => 'GP',
    'Name' => 'Guadeloupe',
  ],
  89 =>
  [
    'Code' => 'GU',
    'Name' => 'Guam',
  ],
  90 =>
  [
    'Code' => 'GT',
    'Name' => 'Guatemala',
  ],
  91 =>
  [
    'Code' => 'GG',
    'Name' => 'Guernsey',
  ],
  92 =>
  [
    'Code' => 'GN',
    'Name' => 'Guinea',
  ],
  93 =>
  [
    'Code' => 'GW',
    'Name' => 'Guinea-Bissau',
  ],
  94 =>
  [
    'Code' => 'GY',
    'Name' => 'Guyana',
  ],
  95 =>
  [
    'Code' => 'HT',
    'Name' => 'Haiti',
  ],
  96 =>
  [
    'Code' => 'HM',
    'Name' => 'Heard Island and McDonald Islands',
  ],
  97 =>
  [
    'Code' => 'VA',
    'Name' => 'Holy See (Vatican City State]',
  ],
  98 =>
  [
    'Code' => 'HN',
    'Name' => 'Honduras',
  ],
  99 =>
  [
    'Code' => 'HK',
    'Name' => 'Hong Kong',
  ],
  100 =>
  [
    'Code' => 'HU',
    'Name' => 'Hungary',
  ],
  101 =>
  [
    'Code' => 'IS',
    'Name' => 'Iceland',
  ],
  102 =>
  [
    'Code' => 'IN',
    'Name' => 'India',
  ],
  103 =>
  [
    'Code' => 'ID',
    'Name' => 'Indonesia',
  ],
  104 =>
  [
    'Code' => 'IR',
    'Name' => 'Iran, Islamic Republic of',
  ],
  105 =>
  [
    'Code' => 'IQ',
    'Name' => 'Iraq',
  ],
  106 =>
  [
    'Code' => 'IE',
    'Name' => 'Ireland',
  ],
  107 =>
  [
    'Code' => 'IM',
    'Name' => 'Isle of Man',
  ],
  108 =>
  [
    'Code' => 'IL',
    'Name' => 'Israel',
  ],
  109 =>
  [
    'Code' => 'IT',
    'Name' => 'Italy',
  ],
  110 =>
  [
    'Code' => 'JM',
    'Name' => 'Jamaica',
  ],
  111 =>
  [
    'Code' => 'JP',
    'Name' => 'Japan',
  ],
  112 =>
  [
    'Code' => 'JE',
    'Name' => 'Jersey',
  ],
  113 =>
  [
    'Code' => 'JO',
    'Name' => 'Jordan',
  ],
  114 =>
  [
    'Code' => 'KZ',
    'Name' => 'Kazakhstan',
  ],
  115 =>
  [
    'Code' => 'KE',
    'Name' => 'Kenya',
  ],
  116 =>
  [
    'Code' => 'KI',
    'Name' => 'Kiribati',
  ],
  117 =>
  [
    'Code' => 'KP',
    'Name' => 'Korea, Democratic People\'s Republic of',
  ],
  118 =>
  [
    'Code' => 'KR',
    'Name' => 'Korea, Republic of',
  ],
  119 =>
  [
    'Code' => 'KW',
    'Name' => 'Kuwait',
  ],
  120 =>
  [
    'Code' => 'KG',
    'Name' => 'Kyrgyzstan',
  ],
  121 =>
  [
    'Code' => 'LA',
    'Name' => 'Lao People\'s Democratic Republic',
  ],
  122 =>
  [
    'Code' => 'LV',
    'Name' => 'Latvia',
  ],
  123 =>
  [
    'Code' => 'LB',
    'Name' => 'Lebanon',
  ],
  124 =>
  [
    'Code' => 'LS',
    'Name' => 'Lesotho',
  ],
  125 =>
  [
    'Code' => 'LR',
    'Name' => 'Liberia',
  ],
  126 =>
  [
    'Code' => 'LY',
    'Name' => 'Libya',
  ],
  127 =>
  [
    'Code' => 'LI',
    'Name' => 'Liechtenstein',
  ],
  128 =>
  [
    'Code' => 'LT',
    'Name' => 'Lithuania',
  ],
  129 =>
  [
    'Code' => 'LU',
    'Name' => 'Luxembourg',
  ],
  130 =>
  [
    'Code' => 'MO',
    'Name' => 'Macao',
  ],
  131 =>
  [
    'Code' => 'MK',
    'Name' => 'Macedonia, the Former Yugoslav Republic of',
  ],
  132 =>
  [
    'Code' => 'MG',
    'Name' => 'Madagascar',
  ],
  133 =>
  [
    'Code' => 'MW',
    'Name' => 'Malawi',
  ],
  134 =>
  [
    'Code' => 'MY',
    'Name' => 'Malaysia',
  ],
  135 =>
  [
    'Code' => 'MV',
    'Name' => 'Maldives',
  ],
  136 =>
  [
    'Code' => 'ML',
    'Name' => 'Mali',
  ],
  137 =>
  [
    'Code' => 'MT',
    'Name' => 'Malta',
  ],
  138 =>
  [
    'Code' => 'MH',
    'Name' => 'Marshall Islands',
  ],
  139 =>
  [
    'Code' => 'MQ',
    'Name' => 'Martinique',
  ],
  140 =>
  [
    'Code' => 'MR',
    'Name' => 'Mauritania',
  ],
  141 =>
  [
    'Code' => 'MU',
    'Name' => 'Mauritius',
  ],
  142 =>
  [
    'Code' => 'YT',
    'Name' => 'Mayotte',
  ],
  143 =>
  [
    'Code' => 'MX',
    'Name' => 'Mexico',
  ],
  144 =>
  [
    'Code' => 'FM',
    'Name' => 'Micronesia, Federated States of',
  ],
  145 =>
  [
    'Code' => 'MD',
    'Name' => 'Moldova, Republic of',
  ],
  146 =>
  [
    'Code' => 'MC',
    'Name' => 'Monaco',
  ],
  147 =>
  [
    'Code' => 'MN',
    'Name' => 'Mongolia',
  ],
  148 =>
  [
    'Code' => 'ME',
    'Name' => 'Montenegro',
  ],
  149 =>
  [
    'Code' => 'MS',
    'Name' => 'Montserrat',
  ],
  150 =>
  [
    'Code' => 'MA',
    'Name' => 'Morocco',
  ],
  151 =>
  [
    'Code' => 'MZ',
    'Name' => 'Mozambique',
  ],
  152 =>
  [
    'Code' => 'MM',
    'Name' => 'Myanmar',
  ],
  153 =>
  [
    'Code' => 'NA',
    'Name' => 'Namibia',
  ],
  154 =>
  [
    'Code' => 'NR',
    'Name' => 'Nauru',
  ],
  155 =>
  [
    'Code' => 'NP',
    'Name' => 'Nepal',
  ],
  156 =>
  [
    'Code' => 'NL',
    'Name' => 'Netherlands',
  ],
  157 =>
  [
    'Code' => 'NC',
    'Name' => 'New Caledonia',
  ],
  158 =>
  [
    'Code' => 'NZ',
    'Name' => 'New Zealand',
  ],
  159 =>
  [
    'Code' => 'NI',
    'Name' => 'Nicaragua',
  ],
  160 =>
  [
    'Code' => 'NE',
    'Name' => 'Niger',
  ],
  161 =>
  [
    'Code' => 'NG',
    'Name' => 'Nigeria',
  ],
  162 =>
  [
    'Code' => 'NU',
    'Name' => 'Niue',
  ],
  163 =>
  [
    'Code' => 'NF',
    'Name' => 'Norfolk Island',
  ],
  164 =>
  [
    'Code' => 'MP',
    'Name' => 'Northern Mariana Islands',
  ],
  165 =>
  [
    'Code' => 'NO',
    'Name' => 'Norway',
  ],
  166 =>
  [
    'Code' => 'OM',
    'Name' => 'Oman',
  ],
  167 =>
  [
    'Code' => 'PK',
    'Name' => 'Pakistan',
  ],
  168 =>
  [
    'Code' => 'PW',
    'Name' => 'Palau',
  ],
  169 =>
  [
    'Code' => 'PS',
    'Name' => 'Palestine, State of',
  ],
  170 =>
  [
    'Code' => 'PA',
    'Name' => 'Panama',
  ],
  171 =>
  [
    'Code' => 'PG',
    'Name' => 'Papua New Guinea',
  ],
  172 =>
  [
    'Code' => 'PY',
    'Name' => 'Paraguay',
  ],
  173 =>
  [
    'Code' => 'PE',
    'Name' => 'Peru',
  ],
  174 =>
  [
    'Code' => 'PH',
    'Name' => 'Philippines',
  ],
  175 =>
  [
    'Code' => 'PN',
    'Name' => 'Pitcairn',
  ],
  176 =>
  [
    'Code' => 'PL',
    'Name' => 'Poland',
  ],
  177 =>
  [
    'Code' => 'PT',
    'Name' => 'Portugal',
  ],
  178 =>
  [
    'Code' => 'PR',
    'Name' => 'Puerto Rico',
  ],
  179 =>
  [
    'Code' => 'QA',
    'Name' => 'Qatar',
  ],
  180 =>
  [
    'Code' => 'RE',
    'Name' => 'Réunion',
  ],
  181 =>
  [
    'Code' => 'RO',
    'Name' => 'Romania',
  ],
  182 =>
  [
    'Code' => 'RU',
    'Name' => 'Russian Federation',
  ],
  183 =>
  [
    'Code' => 'RW',
    'Name' => 'Rwanda',
  ],
  184 =>
  [
    'Code' => 'BL',
    'Name' => 'Saint Barthélemy',
  ],
  185 =>
  [
    'Code' => 'SH',
    'Name' => 'Saint Helena, Ascension and Tristan da Cunha',
  ],
  186 =>
  [
    'Code' => 'KN',
    'Name' => 'Saint Kitts and Nevis',
  ],
  187 =>
  [
    'Code' => 'LC',
    'Name' => 'Saint Lucia',
  ],
  188 =>
  [
    'Code' => 'MF',
    'Name' => 'Saint Martin (French part]',
  ],
  189 =>
  [
    'Code' => 'PM',
    'Name' => 'Saint Pierre and Miquelon',
  ],
  190 =>
  [
    'Code' => 'VC',
    'Name' => 'Saint Vincent and the Grenadines',
  ],
  191 =>
  [
    'Code' => 'WS',
    'Name' => 'Samoa',
  ],
  192 =>
  [
    'Code' => 'SM',
    'Name' => 'San Marino',
  ],
  193 =>
  [
    'Code' => 'ST',
    'Name' => 'Sao Tome and Principe',
  ],
  194 =>
  [
    'Code' => 'SA',
    'Name' => 'Saudi Arabia',
  ],
  195 =>
  [
    'Code' => 'SN',
    'Name' => 'Senegal',
  ],
  196 =>
  [
    'Code' => 'RS',
    'Name' => 'Serbia',
  ],
  197 =>
  [
    'Code' => 'SC',
    'Name' => 'Seychelles',
  ],
  198 =>
  [
    'Code' => 'SL',
    'Name' => 'Sierra Leone',
  ],
  199 =>
  [
    'Code' => 'SG',
    'Name' => 'Singapore',
  ],
  200 =>
  [
    'Code' => 'SX',
    'Name' => 'Sint Maarten (Dutch part]',
  ],
  201 =>
  [
    'Code' => 'SK',
    'Name' => 'Slovakia',
  ],
  202 =>
  [
    'Code' => 'SI',
    'Name' => 'Slovenia',
  ],
  203 =>
  [
    'Code' => 'SB',
    'Name' => 'Solomon Islands',
  ],
  204 =>
  [
    'Code' => 'SO',
    'Name' => 'Somalia',
  ],
  205 =>
  [
    'Code' => 'ZA',
    'Name' => 'South Africa',
  ],
  206 =>
  [
    'Code' => 'GS',
    'Name' => 'South Georgia and the South Sandwich Islands',
  ],
  207 =>
  [
    'Code' => 'SS',
    'Name' => 'South Sudan',
  ],
  208 =>
  [
    'Code' => 'ES',
    'Name' => 'Spain',
  ],
  209 =>
  [
    'Code' => 'LK',
    'Name' => 'Sri Lanka',
  ],
  210 =>
  [
    'Code' => 'SD',
    'Name' => 'Sudan',
  ],
  211 =>
  [
    'Code' => 'SR',
    'Name' => 'Suriname',
  ],
  212 =>
  [
    'Code' => 'SJ',
    'Name' => 'Svalbard and Jan Mayen',
  ],
  213 =>
  [
    'Code' => 'SZ',
    'Name' => 'Swaziland',
  ],
  214 =>
  [
    'Code' => 'SE',
    'Name' => 'Sweden',
  ],
  215 =>
  [
    'Code' => 'CH',
    'Name' => 'Switzerland',
  ],
  216 =>
  [
    'Code' => 'SY',
    'Name' => 'Syrian Arab Republic',
  ],
  217 =>
  [
    'Code' => 'TW',
    'Name' => 'Taiwan, Province of China',
  ],
  218 =>
  [
    'Code' => 'TJ',
    'Name' => 'Tajikistan',
  ],
  219 =>
  [
    'Code' => 'TZ',
    'Name' => 'Tanzania, United Republic of',
  ],
  220 =>
  [
    'Code' => 'TH',
    'Name' => 'Thailand',
  ],
  221 =>
  [
    'Code' => 'TL',
    'Name' => 'Timor-Leste',
  ],
  222 =>
  [
    'Code' => 'TG',
    'Name' => 'Togo',
  ],
  223 =>
  [
    'Code' => 'TK',
    'Name' => 'Tokelau',
  ],
  224 =>
  [
    'Code' => 'TO',
    'Name' => 'Tonga',
  ],
  225 =>
  [
    'Code' => 'TT',
    'Name' => 'Trinidad and Tobago',
  ],
  226 =>
  [
    'Code' => 'TN',
    'Name' => 'Tunisia',
  ],
  227 =>
  [
    'Code' => 'TR',
    'Name' => 'Turkey',
  ],
  228 =>
  [
    'Code' => 'TM',
    'Name' => 'Turkmenistan',
  ],
  229 =>
  [
    'Code' => 'TC',
    'Name' => 'Turks and Caicos Islands',
  ],
  230 =>
  [
    'Code' => 'TV',
    'Name' => 'Tuvalu',
  ],
  231 =>
  [
    'Code' => 'UG',
    'Name' => 'Uganda',
  ],
  232 =>
  [
    'Code' => 'UA',
    'Name' => 'Ukraine',
  ],
  233 =>
  [
    'Code' => 'AE',
    'Name' => 'United Arab Emirates',
  ],
  234 =>
  [
    'Code' => 'GB',
    'Name' => 'United Kingdom',
  ],
  235 =>
  [
    'Code' => 'US',
    'Name' => 'United States',
  ],
  236 =>
  [
    'Code' => 'UM',
    'Name' => 'United States Minor Outlying Islands',
  ],
  237 =>
  [
    'Code' => 'UY',
    'Name' => 'Uruguay',
  ],
  238 =>
  [
    'Code' => 'UZ',
    'Name' => 'Uzbekistan',
  ],
  239 =>
  [
    'Code' => 'VU',
    'Name' => 'Vanuatu',
  ],
  240 =>
  [
    'Code' => 'VE',
    'Name' => 'Venezuela, Bolivarian Republic of',
  ],
  241 =>
  [
    'Code' => 'VN',
    'Name' => 'Viet Nam',
  ],
  242 =>
  [
    'Code' => 'VG',
    'Name' => 'Virgin Islands, British',
  ],
  243 =>
  [
    'Code' => 'VI',
    'Name' => 'Virgin Islands, U.S.',
  ],
  244 =>
  [
    'Code' => 'WF',
    'Name' => 'Wallis and Futuna',
  ],
  245 =>
  [
    'Code' => 'EH',
    'Name' => 'Western Sahara',
  ],
  246 =>
  [
    'Code' => 'YE',
    'Name' => 'Yemen',
  ],
  247 =>
  [
    'Code' => 'ZM',
    'Name' => 'Zambia',
  ],
  248 =>
  [
    'Code' => 'ZW',
    'Name' => 'Zimbabwe',
  ],
];
