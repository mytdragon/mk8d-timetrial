# mk8d-timetrial

## Installation
1. .env file
1. \> composer install
1. \> php artisan migrate
1. \> php artisan db:seed
1. \> php artisan key:generate
1. \> php artisan passport:install

## API Reference

| Bae url |
| --- |
| `http://127.0.0.1/api/` |

| Header name | Required | Header value |
| --- | --- |  --- |  
| Accept | Required | application/json |
| Content-Type | Required | application/json |
| Authorization | Optional | Bearer {access_token} |

### User

#### Signup
Url `/auth/signup`  
Method `POST`

##### Parameters
| Param | Required | Description | Default value | Example |
| --- | --- | :--- | --- | --- |
| email | Required | Email of the user | | dev@mk8d.api |
| password | Required | Password to login. Must be at least 8 caracters | | Pass1234 |
| password_confirmation | Required | Must be equal to `password` param | | Pass1234 |
| pseudo | Require | Name of the player. Must be at least 3 caracters | | Mario |
| country | Require | Can be the id, code (ISO 3166-1 alpha-2) or the name | | CH |

#### Account Activation
Url `/auth/signup/activate/{token}`  
Method `GET`

##### Parameters
| Param | Required | Description | Default value | Example |
| --- | --- | :--- | --- | --- |
| token | Required | Activation's token | | SL3OngqhZvWXlgCdqeOwW2982bPNnQnShH1U8koGrSJp8uh729VXILEj5BRR |

#### Login
Url `/auth/signup/login`  
Method `POST`  

##### Parameters
| Param | Required | Description | Default value | Example |
| --- | --- | :--- | --- | --- |
| email | Required | Email used during signup |  | dev@mk8d.api |
| password | Required | Password used during signup | | Pass1234 |
| remember_me | Optional | Token lasts longer | 0 | 1 |

##### Example Response
```
{
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImUzMTI4NmIyMmRiN2FjMTU3NjgxYzc3M2IyYWQxNjZiOThhNWU3NzNkZDNhMWM5MmVlNDY5Y2ExM2NkN2UwZjNlZmIwMzA5MWUwYzYxYjZmIn0.eyJhdWQiOiIxIiwianRpIjoiZTMxMjg2YjIyZGI3YWMxNTc2ODFjNzczYjJhZDE2NmI5OGE1ZTc3M2RkM2ExYzkyZWU0NjljYTEzY2Q3ZTBmM2VmYjAzMDkxZTBjNjFiNmYiLCJpYXQiOjE1Nzg0ODUxODUsIm5iZiI6MTU3ODQ4NTE4NSwiZXhwIjoxNjEwMTA3NTg1LCJzdWIiOiI2Iiwic2NvcGVzIjpbXX0.gOoB-dRj4rIafQNQUNaNRsaGquHRDINs0vW53zjTBW7MDKv5BUP4dO3ZqKO5uUs_cDtne8mOjVZsFJ2xEybS9ExQ8MsgG00iT9X6eUw8HyjakHvUYISBwDk_nEHo4faU9CfpgKcz96fDJyuzlJOtbSlD1mFlEDr0q48NP6XR6l7TJyVM4rVajHNLM1amiKuIe01LdrvcfdNkfPPF2gzDysLGHrgreUY0QT9LJfLQlMxZqPLlnA77nu1mAb3Oy_mJ23PP4IwZZXvZpr2rIkHvpxX0cE97qOrBxOFJd5iophfQFAmIZvZxikR6pfNsJsnulzzy4T95Gspd_9rCO90f2UIJr7hW8H07507Dm5ix3bxAFXH4CIqVDMWq9muxffbafRFFpGXFp7v7RPKxX1D4Vgu97-Pinoiabh05A9SBGkA8T2FZNBJ5U9yFCl1KQ1_ke9i4Xiw3EY80-eBhsmjKopAIdgNWDTGyat97unXYV830SanVVDq_mQ5Baf817JwokutPXTywB3HWLLTS-xM-80SLzuqf9QKXlguiC-DKgKZlU4v6o_WHxdvPL_8s4CP4kARespy-dydATwiiCpN8bh5flbPALiZx52T9uHTD00N_vB_n-Oh_VsUU-RvbHl51Y68EsuhKhK_q0E4cUsfj1_LdlOZftkOhR7nBC1zs49k",
    "token_type": "Bearer",
    "expires_at": "2020-01-08 13:06:25"
}
```

#### User information
Url `/auth/user`  
Method `GET`  
Requires authentication? `Yes`

##### Example Response
```
{
    "data": {
        "id": 1,
        "email": "dev@mk8d.api",
        "pseudo": "Mario",
        "discord_id": null,
        "switch_fc": null,
        "country": {
        "id": 216,
        "code": "CH",
        "name": "Switzerland"
    },
    "active": 1,
    "is_admin": 0,
    "created_at": "2020-01-08T11:27:55.000000Z",
    "updated_at": "2020-01-08T11:55:00.000000Z"
    }
}
```

### Timetrials

#### Store
Url `/timetrials`  
Method `POST`  
Requires authentication? `Yes`

##### Parameters
| Param | Required | Description | Default value | Example |
| --- | --- | :--- | --- | --- |
| track | Required | MK8dx track. It can be the id, code, or name | | dnbc |
| cc | Required | MK8dx timetrial category. It can only 150 or 200. |  | 150 |
| time | Required | MK8dx time. Format `mm:ss.mss` |  | 1:46.901 |
| splits | Optional | Array of split time. Can be 3 length or exceptionally 7 on GCN Baby Park. |  |  |
| character | Optional | MK8dx character. Can be id or name. |  | Rosalina |
| body | Optional | MK8dx body. Can be id or name. |  | Streetle |
| tire | Optional | MK8dx tire. Can be id or name. |  | Azure Roller |
| glider | Optional | MK8dx glider. Can be id or name. |  | Peach Parasol |
| video | Optional | Link to the video. |  | https://www.youtube.com/watch?v=U9Iwa2q76fI |
| player | Optional | User. Can be a pseudo or id. Require to be admin. |  | dev |

#### Update
Url `/timetrials/{id}`  
Method `POST`  
Requires authentication? `Yes`

Only the player can update his own timetrial or the user need to be admin.

##### Parameters
| Param | Required | Description | Default value | Example |
| --- | --- | :--- | --- | --- |
| track | Optional | MK8dx track. It can be the id, code, or name | | dnbc |
| cc | Optional | MK8dx timetrial category. It can only 150 or 200. |  | 150 |
| time | Optional | MK8dx time. Format `mm:ss.mss` |  | 1:46.901 |
| splits | Optional | Array of split time. Can be 3 length or exceptionally 7 on GCN Baby Park. |  |  |
| character | Optional | MK8dx character. Can be id or name. |  | Rosalina |
| body | Optional | MK8dx body. Can be id or name. |  | Streetle |
| tire | Optional | MK8dx tire. Can be id or name. |  | Azure Roller |
| glider | Optional | MK8dx glider. Can be id or name. |  | Peach Parasol |
| video | Optional | Link to the video. |  | https://www.youtube.com/watch?v=U9Iwa2q76fI |
