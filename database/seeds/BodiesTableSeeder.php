<?php

use Illuminate\Database\Seeder;
use App\Models\Body;

class BodiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bodies = config('mk8dx.bodies');
        foreach ($bodies as $body){
          Body::create(['name' => $body]);
        }
    }
}
