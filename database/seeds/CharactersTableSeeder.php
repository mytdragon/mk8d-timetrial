<?php

use Illuminate\Database\Seeder;
use App\Models\Character;

class CharactersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $characters = config('mk8dx.characters');
        foreach ($characters as $character){
          Character::create(['name' => $character]);
        }
    }
}
