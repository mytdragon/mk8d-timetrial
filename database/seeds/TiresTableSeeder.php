<?php

use Illuminate\Database\Seeder;
use App\Models\Tire;

class TiresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tires = config('mk8dx.tires');
        foreach ($tires as $tire){
          Tire::create(['name' => $tire]);
        }
    }
}
