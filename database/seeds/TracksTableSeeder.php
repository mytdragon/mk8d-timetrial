<?php

use Illuminate\Database\Seeder;
use App\Models\Track;

class TracksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tracks = config('mk8dx.tracks');
        foreach ($tracks as $code => $track){
          Track::create([
              'code' => $code,
              'name' => $track[0],
              'laps' => $track[1]
          ]);
        }
    }
}
