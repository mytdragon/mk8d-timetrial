<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {        
        $this->call(CountriesTableSeeder::class);
        $this->call(CharactersTableSeeder::class);
        $this->call(BodiesTableSeeder::class);
        $this->call(TiresTableSeeder::class);
        $this->call(GlidersTableSeeder::class);
        $this->call(TracksTableSeeder::class);
    }
}
