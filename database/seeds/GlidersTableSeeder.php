<?php

use Illuminate\Database\Seeder;
use App\Models\Glider;

class GlidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gliders = config('mk8dx.gliders');
        foreach ($gliders as $glider){
          Glider::create(['name' => $glider]);
        }
    }
}
