<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimetrialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timetrials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('time', 9);
            $table->string('cc', 3);
            $table->string('video', 255)->nullable();
            $table->timestamps();
            $table->unsignedBigInteger('track_id');
            $table->unsignedBigInteger('character_id')->nullable();
            $table->unsignedBigInteger('body_id')->nullable();
            $table->unsignedBigInteger('tire_id')->nullable();
            $table->unsignedBigInteger('glider_id')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->foreign('track_id')->references('id')->on('tracks')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('character_id')->references('id')->on('characters')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('body_id')->references('id')->on('bodies')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('tire_id')->references('id')->on('tires')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('glider_id')->references('id')->on('gliders')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timetrials');
    }
}
