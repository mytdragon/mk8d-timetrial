# Notes

* CC in timetrial table
* ~~We have 3 tokens, one for desktop, one for mobile and one for the discord bot. On the desktop and mobile clients, the user make a request for the method "Auth" with the client token, user's email and password to get the id of the user. For the discord bot, he make a request to get the user with the discordId. Then, once the client or bot has the id of user, make the request with it.~~ Wont work with Laravel/passport.
  * The idea I have is to create a guard "discord" and think about it ><
* The logout method should remove the id user and the bot should keep in cache the id for a user.
* The request must have "Content-Type" and "Accept" : "application/json"
