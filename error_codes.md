| HTTP  | Code | Error  | Dev  | User |
|---|---|---|---|---|
| 401 |  | InvalidCredentials | Credentials does not match. | Credentials does not match. |
| 401 |  | UserNotActivated | User not activated. | Please verify your email address. |
|  |  |  |  |  |
